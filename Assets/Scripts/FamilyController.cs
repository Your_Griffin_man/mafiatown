﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FamilyController : MonoBehaviour
{
    public List<MafiaFamily> mafiaFamilies;
    // Start is called before the first frame update
    void Start()
    {
		CreateFamily();
    }
    public void CreateFamily()
	{
        foreach (var mafiaFamily in mafiaFamilies)
		{
			mafiaFamily.familyLvl = 3;

			switch (mafiaFamily.nameMafiaFamily)
			{
				case NameMafiaFamily.OPG:
					mafiaFamily.familyName = nameof(NameMafiaFamily.OPG);
					mafiaFamily.familyCapital = 200000;
					mafiaFamily.isPlayer = true;
					break;
				case NameMafiaFamily.Soprano:
					mafiaFamily.familyName = nameof(NameMafiaFamily.Soprano);
					mafiaFamily.familyCapital = 200000;
					mafiaFamily.isPlayer = false;
					break;
				case NameMafiaFamily.Yakudza:
					mafiaFamily.familyName = nameof(NameMafiaFamily.Yakudza);
					mafiaFamily.familyCapital = 200000;
					mafiaFamily.isPlayer = false;
					break;
			}
		}
	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
