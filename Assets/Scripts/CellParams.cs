﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellParams : MonoBehaviour
{
	public bool IsEnabled;
	public TypeCell typeCell;
	public NameMafiaFamily nameMafiaFamily;
	public int customDistrictNum;
	public int preparedDistrictNum;
	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}
}
public enum TypeCell
{
	Decorative,
	Prepared,
	Custom
}
