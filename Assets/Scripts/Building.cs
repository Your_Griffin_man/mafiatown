﻿using UnityEngine;

public class Building : MonoBehaviour
{
	public Renderer MainRenderer;
	public Vector2Int Size = Vector2Int.one;
	public TypeBuilding typeBuilding;
	public string buildingName;
	public int buildingLvl;
	public int buildingMaxLvl;
	public int buildingPrice;
	public int buildingIncome;
	//Принадлежность здания


	private void Start()
	{
		buildingLvl = 1;

		switch(typeBuilding)
		{
			case TypeBuilding.Casino:
				buildingName = nameof(TypeBuilding.Casino);
				buildingMaxLvl = 3;
				buildingPrice = 30000;
				break;
			case TypeBuilding.Brothel:
				buildingName = nameof(TypeBuilding.Brothel);
				buildingMaxLvl = 3;
				buildingPrice = 15000;
				break;
			case TypeBuilding.Armory:
				buildingName = nameof(TypeBuilding.Armory);
				buildingMaxLvl = 5;
				buildingPrice = 6500;
				break;
			case TypeBuilding.Hotel:
				buildingName = nameof(TypeBuilding.Hotel);
				buildingMaxLvl = 5;
				buildingPrice = 4500;
				break;
			case TypeBuilding.AlcoholShop:
				buildingName = "Alcohol Shop";
				buildingMaxLvl = 5;
				buildingPrice = 2500;
				break;
			case TypeBuilding.Restaurant:
				buildingName = nameof(TypeBuilding.Restaurant);
				buildingMaxLvl = 5;
				buildingPrice = 800;
				break;
		}
	}
	public void UpLvlBuilding(int lvlPlayer, int moneyPlayer)
	{
		if(lvlPlayer < buildingLvl || moneyPlayer < buildingPrice || buildingMaxLvl == buildingLvl)
		{
			Debug.Log("Вы не можете улучшить это здание!");
			return;
		}
		buildingLvl++;
		Debug.Log("Вы улучшили это здание !");
	}
	private void OnMouseDown()
	{
		UpLvlBuilding(3, 100000);
	}
	public void SetTransparent(bool available)
	{
		if (available)
		{
			MainRenderer.material.color = Color.green;
		}
		else
		{
			MainRenderer.material.color = Color.red;
		}
	}

	public void SetNormal()
	{
		MainRenderer.material.color = Color.white;
	}

	private void OnDrawGizmos()
	{
		for (int x = 0; x < Size.x; x++)
		{
			for (int y = 0; y < Size.y; y++)
			{
				if ((x + y) % 2 == 0) Gizmos.color = new Color(0.88f, 0f, 1f, 0.3f);
				else Gizmos.color = new Color(1f, 0.68f, 0f, 0.3f);

				Gizmos.DrawCube(transform.position + new Vector3(x, 0, y), new Vector3(1, .1f, 1));
			}
		}
	}
}

public enum TypeBuilding
{
	Casino,
	Brothel,
	Armory,
	Hotel,
	AlcoholShop,
	Restaurant
}