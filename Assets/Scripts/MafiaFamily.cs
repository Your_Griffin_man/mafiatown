﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MafiaFamily 
{
	// Start is called before the first frame update
	public int familyLvl;
	public int familyCapital;
	public string familyName;
	public NameMafiaFamily nameMafiaFamily;
	public bool isPlayer;
	//Принадлежность здания

}
public enum NameMafiaFamily
{
	Soprano,
	OPG,
	Yakudza,
	None,
}
