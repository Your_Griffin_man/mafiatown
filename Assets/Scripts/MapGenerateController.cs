﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MapGenerateController : MonoBehaviour
{
	[Space]
	[Header("CustomDistrictSettings")]
	public int customSize;

	[Space]
	[Header("PreparedDistrictSettings")]
	public int preparedSize;

	[Space]
	[Header("MapSettings")]
	public int height;
	public int width;

	public GameObject cellPrefab;
	public GameObject roadPrefab;
	public GameObject roadVertical;
	public GameObject crossRoadPrefab;
	public List<CellParams> cells;

	public Mesh customBuild;
	public Mesh preparedBuild;
	void Start()
	{
		GenerateMap();
		FilMeshCells();
		DistributionOfBuildings();
	}

	/// <summary>
	///  Генерация карты
	/// </summary>
	public void GenerateMap()
	{
		cells = new List<CellParams>();

		Vector3 cellPosition = Vector3.zero;

		for (int w = 1; w < (width + 1); w++)
		{
			for (int h = 1; h < height; h++)
			{
				var currentCell = Instantiate(cellPrefab, cellPosition, Quaternion.identity);
				var currentCellScript = currentCell.GetComponent<CellParams>();
				currentCellScript.customDistrictNum = GetDistrictNum(h, w, true);
				currentCellScript.preparedDistrictNum = GetDistrictNum(h, w, false);

				cells.Add(currentCellScript);

				CreateRoad(currentCell.GetComponent<BoxCollider>().size.x, cellPosition, h == (height - 1), w == width);

				cellPosition = h == (height - 1) ? new Vector3(cellPosition.x + (currentCell.GetComponent<BoxCollider>().size.x / 1.5f), 0, 0) :
					new Vector3(cellPosition.x, 0, cellPosition.z + (currentCell.GetComponent<BoxCollider>().size.z / 1.5f));
			}
		}
	}

	/// <summary>
	///  Определяет номер района
	/// </summary>
	public int GetDistrictNum(int h, int w, bool isCustom)
	{
		var currentSize = isCustom ? customSize : preparedSize;

		int customHeightDistrictId = h;

		int customHeightDistrictCount = height / currentSize;

		while (customHeightDistrictId > 0)
		{
			customHeightDistrictId -= currentSize;
			customHeightDistrictCount--;
		}

		customHeightDistrictCount = (height / currentSize) - customHeightDistrictCount;

		int customWidthDistrictId = w;

		int customWidthDistrictCount = width / currentSize;

		while (customWidthDistrictId > 0)
		{
			customWidthDistrictId -= currentSize;
			customWidthDistrictCount--;
		}

		customWidthDistrictCount = (width / currentSize) - customWidthDistrictCount;

		customWidthDistrictCount = customWidthDistrictCount == 1 ? 0 : (isCustom ? (customWidthDistrictCount * 5) : 2);

		return customWidthDistrictCount + customHeightDistrictCount;
	}

	/// <summary>
	///  Определяет тип здания
	/// </summary>
	public void FilMeshCells()
	{
		for (int i = 1; i < (cells.Max(c => c.customDistrictNum) + 1); i++)
		{
			var customCells = cells.Where(c => c.customDistrictNum == i).ToList();
			if (!customCells.Any())
				continue;

			var customCell = customCells[Random.Range(0, customCells.Count - 1)];
			customCell.GetComponent<CellParams>().typeCell = TypeCell.Custom;
			customCell.transform.GetChild(0).GetComponent<MeshFilter>().mesh = customBuild;
		}

		for (int i = 1; i < (cells.Max(c => c.preparedDistrictNum) + 1); i++)
		{
			var customCells = cells.Where(c => c.preparedDistrictNum == i && c.typeCell != TypeCell.Custom).ToList();
			if (!customCells.Any())
				continue;

			var customCell = customCells[Random.Range(0, customCells.Count - 1)];
			customCell.GetComponent<CellParams>().typeCell = TypeCell.Prepared;
			customCell.transform.GetChild(0).GetComponent<MeshFilter>().mesh = preparedBuild;
		}

		Debug.Log(cells.Max(c => c.customDistrictNum));
		Debug.Log(cells.Max(c => c.preparedDistrictNum));
	}

	/// <summary>
	///  Генерация дорожной сетки
	/// </summary>
	public void CreateRoad(float cellSize, Vector3 cellPosition, bool isLastHCell, bool isLastWCell)
	{
		if (isLastHCell && isLastWCell)
			return;

		if (!isLastHCell && !isLastWCell)
			Instantiate(crossRoadPrefab,
				new Vector3(cellPosition.x - (cellSize / 50) /* позиция перекрёстков */+ 
				crossRoadPrefab.transform.position.x, crossRoadPrefab.transform.position.y 
				/* позиция перерёстков по высоте y*/, cellPosition.z + (cellSize / 3)), crossRoadPrefab.transform.rotation); // перекрестки

		if (!isLastHCell)
			Instantiate(roadPrefab, new Vector3(cellPosition.x + roadPrefab.transform.position.x, roadPrefab.transform.position.y, cellPosition.z + (cellSize / 3)), roadPrefab.transform.rotation); // дороги по вертикали

		if (!isLastWCell)
			Instantiate(roadVertical, new Vector3(roadVertical.transform.position.x + cellPosition.x - (cellSize / 50), roadVertical.transform.position.y, cellPosition.z + roadVertical.transform.position.z), roadVertical.transform.rotation); // дороги по горизонтали 
	}

	/// <summary>
	/// Распределение зданий между семьями
	/// </summary>
	public void DistributionOfBuildings()
	{
		var familyController = GetComponent<FamilyController>();
		var playerFamily = familyController.mafiaFamilies.FirstOrDefault(c => c.isPlayer).nameMafiaFamily;

		var noneCells = cells.Where(c => c.typeCell == TypeCell.Prepared).ToList();
		DistributionBulding(noneCells, true);

		noneCells = cells.Where(c => c.typeCell == TypeCell.Decorative).ToList();
		DistributionBulding(noneCells, false);

		var customCells = cells.Where(c => c.typeCell == TypeCell.Custom).ToArray();
		DistributionCustomBulding(customCells, NameMafiaFamily.None, Mathf.RoundToInt(customCells.Count() / 2));

		customCells = cells.Where(c => c.typeCell == TypeCell.Custom && c.nameMafiaFamily != NameMafiaFamily.None).ToArray();
		DistributionCustomBulding(customCells, playerFamily, customCells.Count() / 5);

		NameMafiaFamily? nameMafiaFamily = null;
		foreach (var familyName in familyController.mafiaFamilies.Where(c => !c.isPlayer).Select(c => c.nameMafiaFamily).Reverse())
		{
			customCells = cells.Where(c => c.typeCell == TypeCell.Custom && c.nameMafiaFamily != NameMafiaFamily.None && c.nameMafiaFamily != playerFamily && ((nameMafiaFamily == null ) || (nameMafiaFamily != null && c.nameMafiaFamily != nameMafiaFamily))).ToArray();
			DistributionCustomBulding(customCells, familyName, customCells.Count() / 2);

			if (nameMafiaFamily == null)
				nameMafiaFamily = familyName;
		}
	}

	/// <summary>
	/// Распределение зданий между семьями
	/// </summary>
	public void DistributionCustomBulding(CellParams[] customCells, NameMafiaFamily nameMafiaFamily, int newSize)
	{
		System.Array.Resize(ref customCells, newSize);
		foreach (var customCell in customCells)
		{
			customCell.nameMafiaFamily = nameMafiaFamily;
			customCell.IsEnabled = true;
		}
	}
	public void DistributionBulding(List<CellParams> decorativeCells, bool isEnable)
	{
		foreach (var decorativeCell in decorativeCells)
		{
			decorativeCell.nameMafiaFamily = NameMafiaFamily.None;
			decorativeCell.IsEnabled = isEnable;
		}
	}
}